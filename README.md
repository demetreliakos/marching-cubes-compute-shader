# Marching Cubes Compute Shader

Implementation of the marching cubes algorithm on the GPU using a compute shader in Unity3D

## Usage and Controls

* WASD for movement
* Mouse for look
* Hold shift to move fast
* G toggle's gravity
* Space to jump
* Control to crouch