﻿using System;
using UnityEngine;

[Serializable]
public class FluidLook
{

    public float XSensitivity = 2f;
    public float YSensitivity = 2f;
    public bool ClampVerticalRotation = true;
    [Range(-90f, 90f)] public float MinimumX = -90f;
    [Range(-90f, 90f)] public float MaximumX = 90f;
    public bool Smooth = false;
    public float SmoothSpeed = 15f;
    public bool LockCursor = true;
    
    private Quaternion _characterTargetRotation;
    private Quaternion _cameraTargetRotation;
    private bool _cursorIsLocked = true;
    private Transform _character;
    private Transform _camera;
    private float _minimumXRadClamp;
    private float _maximumXRadClamp;

    public void Init(Transform character, Transform camera)
    {
        _characterTargetRotation = character.localRotation;
        _cameraTargetRotation = camera.localRotation;
        _character = character;
        _camera = camera;
}

    public void UpdateLook()
    {

        UpdateCursorLock();
        if (!_cursorIsLocked) return;

        float yRotation = Input.GetAxis("Mouse X") * XSensitivity;
        float xRotation = Input.GetAxis("Mouse Y") * YSensitivity;

        _characterTargetRotation *= Quaternion.Euler(0f, yRotation, 0f);
        _cameraTargetRotation *= Quaternion.Euler(-xRotation, 0f, 0f);

        if (ClampVerticalRotation)
        {
            _cameraTargetRotation = ClampRotationAroundXAxis(_cameraTargetRotation);
        }
        
        if (Smooth)
        {
            _character.localRotation = Quaternion.Slerp(
                _character.localRotation, _characterTargetRotation, SmoothSpeed * Time.deltaTime);
            _camera.localRotation = Quaternion.Slerp(
                _camera.localRotation, _cameraTargetRotation, SmoothSpeed * Time.deltaTime);
        }
        else
        {
            _character.localRotation = _characterTargetRotation;
            _camera.localRotation = _cameraTargetRotation;
        }

    }

    // Set whether or not to lock the cursor
    public void SetCursorLock(bool value)
    {
        LockCursor = value;
        if (!LockCursor)
        {
            // Immediately disable cursor locking
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
            _cursorIsLocked = false;
        }
    }

    private void UpdateCursorLock()
    {
        // This ensures the cursor is always locked
        if (LockCursor)
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                _cursorIsLocked = false;
            }
            else if (Input.GetMouseButtonUp(0))
            {
                _cursorIsLocked = true;
            }

            if (_cursorIsLocked)
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
            else if (!_cursorIsLocked)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }
    }

    private Quaternion ClampRotationAroundXAxis(Quaternion q)
    {
        _minimumXRadClamp = 0.5f * Mathf.Deg2Rad * MinimumX;
        _maximumXRadClamp = 0.5f * Mathf.Deg2Rad * MaximumX;
        q.x = q.w*Mathf.Tan(Mathf.Clamp(Mathf.Atan(q.x / q.w), _minimumXRadClamp, _maximumXRadClamp));
        return q;
    }

}
