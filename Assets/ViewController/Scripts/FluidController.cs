﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(AudioSource))]
public class FluidController : MonoBehaviour
{

    //[SerializeField] private bool _isWalking;
    //[SerializeField] private bool _isRunning;
    //[SerializeField] private bool _isCrouching;
    [SerializeField] private float _walkSpeed = 4f;
    [SerializeField] private float _runSpeed = 8f;
    [SerializeField] private float _crouchSpeed = 1.5f;
    [SerializeField] private float _walkHeight = 2f;
    [SerializeField] private float _runHeight = 1.5f;
    [SerializeField] private float _crouchHeight = 1f;
    [SerializeField] private float _jumpHeight = 0.5f;
    [SerializeField] private float _cameraOffset = 0.5f;
    [SerializeField] private float _stickToGroundForce = 10f;
    [SerializeField] private bool _enableGravity = true;
    [SerializeField] private FluidLook _lookControl = new FluidLook();
    [SerializeField] private FluidInput _input = new FluidInput();

    private Camera _camera;
    private AudioSource _audioSource;
    private CharacterController _characterController;

    private Vector3 _velocity;
    private CollisionFlags _collisionFlags;
    private float _targetSpeed;
    private float _targetHeight;
    private float _ungroundedJumpOffset;
    private bool _wasGroundedOnCrouchAction;
    private float _crouchDownSpeed;
    private float _lastYPos;
    private float _deltaYPos;

    // Start is called before the first frame update
    private void Start()
    {
        _characterController = GetComponent<CharacterController>();
        _camera = Camera.main;
        _audioSource = GetComponent<AudioSource>();
        _lookControl.Init(transform, _camera.transform);
        _velocity = Vector3.zero;
        _crouchDownSpeed = Mathf.Sqrt((_walkHeight - _crouchHeight) * (2f * Physics.gravity.magnitude));
    }

    // Update is called once per frame
    private void Update()
    {
        _lookControl.UpdateLook();
        _input.CaptureInput();

        // DEBUG: toggle gravity
        if (Input.GetKeyDown(KeyCode.G)) {
            _enableGravity = !_enableGravity;
        }

    }

    // Called once per physics frame
    private void FixedUpdate()
    {

        UpdateTargetSpeedAndHeight();
        UpdateVelocity();
        UpdateHeight();

        // Apply movement and clear input buffer
        _lastYPos = _characterController.transform.position.y;
        _collisionFlags = _characterController.Move(_velocity * Time.fixedDeltaTime - transform.up * _ungroundedJumpOffset);
        _input.ClearInput();

    }

    private void UpdateTargetSpeedAndHeight()
    {
        if (_input.Crouch && _input.Sprint)
        {
            _targetSpeed = _walkSpeed;
            _targetHeight = _crouchHeight;
        }
        else if (_input.Crouch)
        {
            _targetSpeed = _crouchSpeed;
            _targetHeight = _crouchHeight;
        }
        else if (_input.Sprint)
        {
            _targetSpeed = _runSpeed;
            _targetHeight = _runHeight;
        }
        else
        {
            _targetSpeed = _walkSpeed;
            _targetHeight = _walkHeight;
        }

        if (!_characterController.isGrounded && _velocity.y > 0)
        {
            _targetHeight = _crouchHeight;
        }
    }

    private void UpdateVelocity()
    {
        if (_enableGravity)
        {

            // Determine the movement
            Vector3 desiredMove = transform.forward * _input.Move.y + transform.right * _input.Move.x;
            _velocity.x = desiredMove.x * _targetSpeed;
            _velocity.z = desiredMove.z * _targetSpeed;

            // Jump or apply gravity
            if (_characterController.isGrounded)
            {
                if (_input.Jump)
                {
                    _deltaYPos = Mathf.Max((_characterController.transform.position.y - _lastYPos) / Time.deltaTime, 0);
                    _velocity.y = Mathf.Sqrt(2 * Physics.gravity.magnitude * _jumpHeight) + _deltaYPos;
                }
                else
                {
                    Physics.SphereCast(transform.position, _characterController.radius, Vector3.down, out RaycastHit hitInfo,
                                       _characterController.height / 2f, Physics.AllLayers, QueryTriggerInteraction.Ignore);
                    _velocity.y = -_stickToGroundForce * Vector3.Dot(hitInfo.normal, Vector3.up);
                }
            }
            else
            {
                _velocity.y += Physics.gravity.y * Time.fixedDeltaTime;
            }

        }
        else
        {
            // Determine the movement
            Vector3 desiredMove = _camera.transform.forward * _input.Move.y + transform.right * _input.Move.x;
            _velocity = desiredMove * _targetSpeed;
        }
    }

    private void UpdateHeight()
    {
        if (Mathf.Approximately(_characterController.height, _targetHeight) || _input.Jump)
        {
            _wasGroundedOnCrouchAction = false;
        }
        else if (_characterController.isGrounded)
        {
            _wasGroundedOnCrouchAction = true;
        }

        float newHeight = Mathf.MoveTowards(_characterController.height, _targetHeight, _crouchDownSpeed * Time.deltaTime);
        float deltaHeight = newHeight - _characterController.height;
        _ungroundedJumpOffset = (!_wasGroundedOnCrouchAction) ? deltaHeight : 0;
        _characterController.height = newHeight;
        _characterController.center = new Vector3(0, -(_walkHeight - newHeight) * 0.5f, 0);
        _camera.transform.localPosition = new Vector3(0, newHeight - _cameraOffset - _walkHeight * 0.5f, 0);

    }

}
