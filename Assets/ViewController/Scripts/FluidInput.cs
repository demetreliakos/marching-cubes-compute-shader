﻿using System;
using UnityEngine;

[Serializable]
public class FluidInput
{

    public bool AllowBunnyHop = false;

    public bool Jump { get; private set; }
    public bool JumpHold { get; private set; }
    public bool Crouch { get; private set; }
    public bool Sprint { get; private set; }
    public Vector2 Move { get; private set; }

    public void CaptureInput()
    {
        JumpHold = Input.GetKey(KeyCode.Space);
        Jump |= (AllowBunnyHop) ? JumpHold : Input.GetKeyDown(KeyCode.Space);
        Crouch = Input.GetKey(KeyCode.LeftControl);
        Sprint = Input.GetKey(KeyCode.LeftShift);
        Move = Vector2.ClampMagnitude(new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")), 1);
    }

    public void ClearInput()
    {
        if (AllowBunnyHop) return;
        Jump = false;
    }

}
