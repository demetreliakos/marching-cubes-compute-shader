﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshCollider))]
[RequireComponent(typeof(MeshRenderer))]
public class MeshBuilder : MonoBehaviour {

    public Gradient TerrainGradient; // TODO: this should be somewhere else
    public bool GizmoRenderVoxelDensity = true;
    public uint EdgeChunkFlags = 0;

    // Chunk size
    private const int _SIZE = 32;

    // Voxel terrain data
    private float[,,] _voxels;
    private int       _verticesCount;
    private int       _trianglesCount;
    private Vector3[] _vertices;
    private int[]     _triangles;
    private Color[] _colors;

    // The physical unity mesh
    private Mesh _mesh;
    private MeshCollider _meshCollider;
    private MeshBuilderComputeWrapper _computeWrapper;
    
    // Start is called before the first frame update
    void Start() {

        // Get mesh
        _mesh = GetComponent<MeshFilter>().mesh;
        _meshCollider = GetComponent<MeshCollider>();
        _mesh.MarkDynamic();

        // _mesh.indexFormat = IndexFormat.UInt32;

        // Initialise the compute shader wrapper
        _computeWrapper = new MeshBuilderComputeWrapper(_SIZE);

        // Setup the terrain
        InitialiseBuffers();
        //GenerateVoxelData();
        GenerateVoxelDataAlternate();
        UpdateMesh();

    }

    private void InitialiseBuffers() {
        // Triangles maps vertices to a triangle
        _triangles = new int[_SIZE*_SIZE*_SIZE*5*3];
        _colors = new Color[_SIZE*_SIZE*_SIZE*5*3];
        for (int i = 0; i < _SIZE*_SIZE*_SIZE*5*3; i++) {
            _triangles[i] = i;
            _colors[i] = new Color(0,0,0.5f);/*new Color(
                                 (byte)UnityEngine.Random.Range(0, 255),
                                 (byte)UnityEngine.Random.Range(0, 255),
                                 (byte)UnityEngine.Random.Range(0, 255),
                                 255);*/
        }
        // Other terrain data buffers are allocated here
        _voxels = new float[_SIZE+1, _SIZE+1, _SIZE+1];
        _vertices = new Vector3[_SIZE*_SIZE*_SIZE*5*3];
    }

    private void GenerateVoxelData() {
        for (int i = 0; i < _SIZE+1; i++) {
            for (int j = 0; j < _SIZE+1; j++) {
                for (int k = 0; k < _SIZE+1; k++) {
                    /*if (i==0 || j==0 || k==0 || i==_size || j==_size || k==_size) {
                        _voxels[i, j, k] = 0;
                        continue;
                    }*/
                    if (j==0 || j==_SIZE) {
                        _voxels[i, j, k] = 0;
                        continue;
                    }
                    //float sample = Mathf.PerlinNoise((_DEBUG_4d+(float)i) / 16f, ((float)k) / 30f);
                    float sample = Mathf.PerlinNoise((transform.position.x+(float)i) / 50f, (transform.position.z+(float)k) / 102f);
                    _voxels[i, j, k] = Mathf.Clamp(_SIZE*(sample-(j+0.5f)/_SIZE), -1, 1);
                }
            }
        }
    }

    private void GenerateVoxelDataAlternate() {
        for (int i = 0; i < _SIZE+1; i++) {
            for (int j = 0; j < _SIZE+1; j++) {
                for (int k = 0; k < _SIZE+1; k++) {
                    if ((EdgeChunkFlags&1) > 0 && i==0) {
                        _voxels[i, j, k] = 0;
                        continue;
                    }
                    if ((EdgeChunkFlags&2) > 0 && k==0) {
                        _voxels[i, j, k] = 0;
                        continue;
                    }
                    if ((EdgeChunkFlags&4) > 0 && i==_SIZE) {
                        _voxels[i, j, k] = 0;
                        continue;
                    }
                    if ((EdgeChunkFlags&8) > 0 && k==_SIZE) {
                        _voxels[i, j, k] = 0;
                        continue;
                    }

                    if (j==0) {
                        _voxels[i, j, k] = 1;
                        continue;
                    }
                    if (j==_SIZE) {
                        _voxels[i, j, k] = 0;
                        continue;
                    }
                    float relX = transform.position.x+i;
                    float relY = transform.position.y+j;
                    float relZ = transform.position.z+k;
                    float groundPlane = 0.85f*_SIZE/2-j;
                    float mountains = Mathf.Clamp(2*_SIZE*Perlin3D((relX-266)/32, (relY+5406)/16, (relZ+1060)/32), 0, float.PositiveInfinity);
                    float mountains2 = Mathf.Clamp(0.5f*_SIZE*Perlin3D((relX+40)/14, (relY-750)/24, (relZ-90)/14), 0, float.PositiveInfinity);
                    float valleys = Mathf.Clamp(2*_SIZE*Perlin3D((relX+10)/32, (relY+1254)/8, (relZ-58051)/32), float.NegativeInfinity, 0);
                    float sample = groundPlane+mountains+mountains2+valleys;
                    _voxels[i, j, k] = Mathf.Clamp(0.25f*sample, -1, 1);
                }
            }
        }
    }

    private void GenerateVoxelDataAlternate2() {
        for (int i = 0; i < _SIZE+1; i++) {
            for (int j = 0; j < _SIZE+1; j++) {
                for (int k = 0; k < _SIZE+1; k++) {
                    /*if (i==0 || j==0 || k==0 || i==_size || j==_size || k==_size) {
                        _voxels[i, j, k] = 0;
                        continue;
                    }*/
                    if (j==0 || j==_SIZE) {
                        _voxels[i, j, k] = 0;
                        continue;
                    }
                    //float sample = Perlin3D((_DEBUG_4d+(float)i) / 16f, ((float)j) / 10f, ((float)k) / 30f);
                    float sample = Perlin3D((transform.position.x+(float)i) / 16f, (transform.position.y+(float)j) / 10f, (transform.position.z+(float)k) / 30f);
                    //float sample = Mathf.PerlinNoise((_DEBUG_4d+(float)i) / 16f, ((float)k) / 30f);
                    _voxels[i, j, k] = Mathf.Clamp((sample-0.5f), -1, 1);
                }
            }
        }
    }

    private float Perlin3D(float x, float y, float z) {
        float xy = Mathf.PerlinNoise(0.95f*x+0.5f, y);
        float yz = Mathf.PerlinNoise(y-0.156f, z-560f);
        float zx = Mathf.PerlinNoise(1.21f*z+460.120f, x);
        float yx = Mathf.PerlinNoise(y, x-20.51f);
        float zy = Mathf.PerlinNoise(z-0.20210f, y-0.2026f);
        float xz = Mathf.PerlinNoise(1.0215f*x+564.50f, z-0.98404f);
        return (xy+yz+zx+yx+zy+xz)/6f-0.5f;
    }

    private void UpdateMesh() {

        // Build the mesh on the GPU using marching cubes
        Vector2Int counts = _computeWrapper.GenerateVertices(_voxels, ref _vertices, ref _triangles);
        _verticesCount = counts.x;
        _trianglesCount = counts.y;

        // TODO: this should be part of the shader for performance
        for (int i = 0; i < _verticesCount; i++) {
            _colors[i] = TerrainGradient.Evaluate(_vertices[i].y/_SIZE);
        }
        //for (int i = 0; i < _verticesCount; i++) {
        //    _colors[i] = new Color(_vertices[i].y/_SIZE, 0, 0.5f);
        //}

        // Copy over only the parts of the buffer with valid data
        Vector3[] meshVertices = new Vector3[_verticesCount];
        int[] meshTriangles = new int[_trianglesCount];
        Color[] meshColors = new Color[_verticesCount];
        Array.Copy(_vertices, 0, meshVertices, 0, _verticesCount);
        Array.Copy(_triangles, 0, meshTriangles, 0, _trianglesCount);
        Array.Copy(_colors, 0, meshColors, 0, _verticesCount);
        
        // Update the mesh
        _mesh.Clear();
        _mesh.vertices = meshVertices;
        _mesh.triangles = meshTriangles;
        _mesh.colors = meshColors;
        _mesh.RecalculateNormals();
        //_mesh.RecalculateTangents();
        //_mesh.RecalculateBounds();

        // Update the mesh collider
        _meshCollider.sharedMesh = null;
        _meshCollider.sharedMesh = _mesh;

    }
    
    // Update is called once per frame
    void FixedUpdate() {
        //_DEBUG_4d += 0.5f;
        //GenerateVoxelData();
        //GenerateVoxelDataAlternate();
        //UpdateMesh();
    }

    void OnDestroy() {
        _computeWrapper.Release();
    }

    // Draw a wire cube mesh around our sample space to visualize it
    void OnDrawGizmos() {
        Vector3 cubeSize = new Vector3(_SIZE, _SIZE, _SIZE);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position + cubeSize * 0.5f, cubeSize);
        Vector3 solidSize = Vector3.one*1f;
        if (GizmoRenderVoxelDensity && _voxels!=null) {
            for (int i = 0; i < _SIZE+1; i++) {
                for (int j = 0; j < _SIZE+1; j++) {
                    for (int k = 0; k < _SIZE+1; k++) {
                        // Centre of the current voxel
                        Vector3 centre = new Vector3(i + 0.5f, j + 0.5f, k + 0.5f);
                        float intensity = _voxels[i, j, k];
                        //Gizmos.color = new Color(intensity, 1- intensity, 1);
                        Gizmos.color = Color.HSVToRGB(intensity*0.7f, 1, 1);
                        Gizmos.color = new Color(Gizmos.color.r, Gizmos.color.g, Gizmos.color.b, 0.25f);
                        // DEBUG: add solid
                        if (intensity > 0 && intensity < 0.99) {
                            Gizmos.DrawCube(transform.position + centre - solidSize * 0.5f, solidSize);
                        }
                    }
                }
            }
        }
    }
}
