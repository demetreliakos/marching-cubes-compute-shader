﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeshBuilderComputeWrapper {

    // Data flow structure
    //
    // ----------------------------------------------------------------------------------------
    //   Kernel    | IO  | DataStructure | Element        | Description
    // ----------------------------------------------------------------------------------------
    //   GenVerts  | in  | Voxels        | float          | Array of voxel weights
    //             | out | Cases         | z8_y8_x8_case8 | List of nonempty cases
    //             | out | Verts         | float3         | List of verts
    //             | out | SplatVert     | uint           | Array of vert indices for each vert
    // ----------------------------------------------------------------------------------------
    //   GenTris   | in  | SplatVert     | uint           | Array of vert indices for each vert
    //             | in  | Cases         | z8_y8_x8_case8 | List of nonempty cases
    //             | out | Tris          | uint           | List of 3 vert indices for every triangle
    // ----------------------------------------------------------------------------------------

    // Chunk size
    readonly int _size;
    readonly int _sizeBuff;

    // Compute shader model
    private ComputeShader _computeShader;

    // Buffers
    private ComputeBuffer _csInput_Voxels; // I
    private ComputeBuffer _csInput_NumVerts; // I
    private ComputeBuffer _csBuffer_Cases; // IO
    private ComputeBuffer _csBuffer_SplatVerts; // IO
    private ComputeBuffer _csOutput_Verts; // O
    private ComputeBuffer _csOutput_Tris; // O
    private ComputeBuffer _csOutput_CountReader; // O

    // Kernels
    private int _csKernel_GenVerts;
    private int _csKernel_GenTris;

    // Readonly writeable buffer to get vertices
    private readonly int[] _singleValueReadBuffer;

    public MeshBuilderComputeWrapper(int size) {

        // Setup size (cubic)
        _size = size;
        _sizeBuff = size+2;
        _singleValueReadBuffer = new int[1];

        // Load the compute shader from the resources folder
        _computeShader = (ComputeShader)Object.Instantiate(Resources.Load("Shaders/MeshBuilderCompute"));

        // Get the kernel IDs
        _csKernel_GenVerts = _computeShader.FindKernel("GenVerts");
        _csKernel_GenTris = _computeShader.FindKernel("GenTris");

        // Allocate the compute buffers
        _csInput_NumVerts = new ComputeBuffer(1, sizeof(int), ComputeBufferType.Raw);
        _csInput_Voxels = new ComputeBuffer(_sizeBuff*_sizeBuff*_sizeBuff, sizeof(float), ComputeBufferType.Default);
        _csBuffer_Cases = new ComputeBuffer(_size*_size*_size, sizeof(uint), ComputeBufferType.Counter);
        _csBuffer_SplatVerts = new ComputeBuffer(_sizeBuff*_sizeBuff*_sizeBuff, sizeof(uint), ComputeBufferType.Default);
        _csOutput_Verts = new ComputeBuffer(_size*_size*_size, sizeof(float)*3, ComputeBufferType.Default);
        _csOutput_Tris = new ComputeBuffer(_size*_size*_size*3, sizeof(uint)*6, ComputeBufferType.Counter);
        _csOutput_CountReader = new ComputeBuffer(1, sizeof(int), ComputeBufferType.IndirectArguments);

        // Prepare the shader IO
        _computeShader.SetBuffer(_csKernel_GenVerts, "Voxels", _csInput_Voxels);
        _computeShader.SetBuffer(_csKernel_GenVerts, "Cases", _csBuffer_Cases);
        _computeShader.SetBuffer(_csKernel_GenVerts, "Verts", _csOutput_Verts);
        _computeShader.SetBuffer(_csKernel_GenVerts, "SplatVerts", _csBuffer_SplatVerts);
        // Prepare the shader IO
        _computeShader.SetBuffer(_csKernel_GenTris,   "NumVerts", _csInput_NumVerts);
        _computeShader.SetBuffer(_csKernel_GenTris,   "SplatVerts", _csBuffer_SplatVerts);
        _computeShader.SetBuffer(_csKernel_GenTris,   "Cases", _csBuffer_Cases);
        _computeShader.SetBuffer(_csKernel_GenTris,   "Tris", _csOutput_Tris);

        // Also set all buffers initially
        _csInput_Voxels.SetData(new float[_sizeBuff*_sizeBuff*_sizeBuff]);
        _csInput_NumVerts.SetData(new int[1]);
        _csBuffer_Cases.SetData(new uint[_size*_size*_size]);
        _csBuffer_SplatVerts.SetData(new uint[_sizeBuff*_sizeBuff*_sizeBuff]);
        _csOutput_Verts.SetData(new float[_size*_size*_size*3]);
        _csOutput_Tris.SetData(new uint[_size*_size*_size*3*6]);
        _csOutput_CountReader.SetData(new int[1]);

    }

    // Start is called before the first frame update
    public Vector2Int GenerateVertices(in float[,,] voxels, ref Vector3[] vertices, ref int[] triangles) {

        // Reset input output buffers
        _csInput_Voxels.SetData(voxels);
        _csBuffer_Cases.SetCounterValue(0);
        _csOutput_Tris.SetCounterValue(0);

        // Make shader calls
        _computeShader.Dispatch(_csKernel_GenVerts, (_size)/8, (_size)/8, _size/8);
        ComputeBuffer.CopyCount(_csBuffer_Cases, _csInput_NumVerts, 0);
        ComputeBuffer.CopyCount(_csBuffer_Cases, _csOutput_CountReader, 0);
        _csOutput_CountReader.GetData(_singleValueReadBuffer);
        int verticesCount = _singleValueReadBuffer[0];
        if (verticesCount==0) return Vector2Int.zero;
        _computeShader.Dispatch(_csKernel_GenTris, verticesCount/256+1, 1, 1);

        // Get the number of tries generated
        ComputeBuffer.CopyCount(_csOutput_Tris, _csOutput_CountReader, 0);
        _csOutput_CountReader.GetData(_singleValueReadBuffer);
        int trianglesCount = _singleValueReadBuffer[0] * 6;

        // Get the vertices generated
        _csOutput_Verts.GetData(vertices, 0, 0, verticesCount);
        _csOutput_Tris.GetData(triangles, 0, 0, trianglesCount);

        // Return the number of vertices generated
        return new Vector2Int(verticesCount, trianglesCount);
    }

    public void Release() {
        _csInput_Voxels.Release();
        _csInput_NumVerts.Release();
        _csBuffer_Cases.Release();
        _csBuffer_SplatVerts.Release();
        _csOutput_Verts.Release();
        _csOutput_Tris.Release();
        _csOutput_CountReader.Release();
    }

}
