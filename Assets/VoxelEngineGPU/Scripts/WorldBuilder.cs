﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBuilder : MonoBehaviour {

    private const int _GRID_SIZE = 6;
    private const int _CHUNK_SIZE = 32;

    private GameObject[,] _chunks;
    public GameObject VoxelMeshRendererPrefab;

    // Start is called before the first frame update
    void Start() {

        _chunks = new GameObject[_GRID_SIZE, _GRID_SIZE];
        for (int i = 0; i < _GRID_SIZE; i++) {
            for (int k = 0; k < _GRID_SIZE; k++) {
                _chunks[i,k] = Instantiate(VoxelMeshRendererPrefab, new Vector3(i*(_CHUNK_SIZE-1), 0, k*(_CHUNK_SIZE-1)), Quaternion.identity);
                MeshBuilder mb = _chunks[i, k].GetComponent<MeshBuilder>();
                if (i==0) mb.EdgeChunkFlags |= 1;
                if (k==0) mb.EdgeChunkFlags |= 2;
                if (i==_GRID_SIZE-1) mb.EdgeChunkFlags |= 4;
                if (k==_GRID_SIZE-1) mb.EdgeChunkFlags |= 8;
            }
        }

    }

    // Update is called once per frame
    void Update() {

    }
}
